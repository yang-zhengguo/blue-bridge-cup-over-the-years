//A.九进制转十进制
/*问题描述
九进制正整数(2022)₉转换成十进制等于多少？*/
#include <bits/stdc++.h>
using namespace std;
 
const int N = 1e5 + 10;
int main()
{
	int x = 2022;
	int a = 1;
	int ans = 0;
	while(x) {
		ans += (x % 10) * a;
		a = a * 9;
		x /= 10;
	}
	cout << ans;
 
	return 0;
}